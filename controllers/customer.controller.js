const costumer = require('../models/customers');
exports.create = async (req, res) =>{
    const cst = new costumer({
        name : req.body.name,
        age : req.body.age
    });

    //save cst into db

    await cst
    .save()
    .then((data) => {
        res.status(201).send({data})
    })
    .catch(err=>{
        res.status(500).send({
            message:
            err.message || 'test error'
        });
    });
};

exports.findAll = async(req, res) =>{
    await costumer.find()
    .then((data) => {
        res.status(201).send({data})
    })
    .catch(err=>{
        res.status(500).send({
            message:
            err.message || 'test error'
        });
    });
};

exports.findOne = async(req, res) =>{
    await costumer.findOne()
    .then((data) => {
        res.status(201).send({data})
    })
    .catch(err=>{
        res.status(500).send({
            message:
            err.message || 'test error'
        });
    });
};

exports.update = async(req, res) =>{
    await costumer.updateMany()
    .then((data) => {
        res.status(201).send({data})
    })
    .catch(err=>{
        res.status(500).send({
            message:
            err.message || 'test error'
        });
    });
};

exports.delete = async(req, res) =>{
    await costumer.deleteOne()
    .then((data) => {
        res.status(201).send({data})
    })
    .catch(err=>{
        res.status(500).send({
            message:
            err.message || 'test error'
        });
    });
};